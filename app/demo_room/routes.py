from app.demo_room import bp
from app.extensions import db
from app.models.roomState import RoomState, RoomStateIn, RoomStateOut
from app.models.player import Player
from app.models.object import Object, ObjectOut  # Import ObjectOut here
from app.models.objectState import ObjectState
from app.models.schemas import PingOut, DescriptionOut, ImageOut, ExitsOut, TokenOut, InteractionIn

from flask import current_app

from app.auth import token_auth


def create_room_objects():
    # Room Objects
    objects = [
        {
            'name': 'Slot Machine',
            'description': 'A slot machine with a hint of a winning combination.',
            'active': True,
            'icon': 'slot-machine'
        },
        {
            'name': 'Poker Table',
            'description': 'A poker table with a note on it.',
            'active': True,
            'icon': 'table'
        },
        {
            'name': 'Croupier',
            'description': 'A croupier ready to play blackjack with you.',
            'active': True,
            'icon': 'person'
        },
        {
            'name': 'Painting',
            'description': 'A painting with a hidden safe behind it.',
            'active': True,
            'icon': 'painting'
        },
        {
            'name': 'Roulette Wheel',
            'description': 'A roulette wheel with a challenge to bet on the correct number.',
            'active': True,
            'icon': 'roulette-wheel'
        }
    ]

    # check if we already have objects in the DB
    cur_objs = Object.query.all()

    # if not create the objects initially
    if not cur_objs:
        for obj_data in objects:
            object = Object(**obj_data)
            db.session.add(object)
            db.session.commit()


@bp.get('/objects')
@bp.output(ObjectOut(many=True), status_code=200)
def get_objects():
    objects = Object.query.all()
    return objects


@bp.get('/ping')
@bp.output(PingOut, status_code=200)
def get_ping():
    return {"message": "pong"}

@bp.get('/description')
@bp.output(DescriptionOut, status_code=200)
def get_description():
    return {
        "description":
        """You wake up in a luxurious casino room. Plush carpets, slot machines, poker tables, and a locked exit door surround you."""
    }

@bp.get('/image')
@bp.output(ImageOut, status_code=200)
def get_image():
    return current_app.send_static_file('casino_room.png')

@bp.get('/exits')
@bp.output(ExitsOut, status_code=200)
def get_exits():
    # Describe the exits as seen from outside the room
    return {
        "north": "A golden door with intricate designs.",
        "east": "A silver door with the word 'Exit' engraved.",
        "south": "A heavy door with casino symbols.",
        "west": "A glass door revealing more casino games."
    }

@bp.post('/start')
@bp.input(RoomStateIn, location='json')
@bp.output(TokenOut, status_code=200)
def start_visit(json_data):
    # Get the player email from the nested JSON data
    player_email = json_data.get('player', {}).get('email')
    # search player by email in database (TODO: does not work - each time a new player is generated)
    player = Player.query.filter_by(email=player_email).first()

    # if player does not exist, create a db entry
    if not player:
        player = Player(**json_data.get('player'))
        db.session.add(player)
    
    # create a roomState object for the current visit
    roomState = RoomState(json_data.get('game_uuid'))
    roomState.player = player
        
    # create states for all objects
    objects = Object.query.all()
    visibility = False
    locked = False
    for obj in objects:
        match obj.name:
            case 'Slot Machine':
                visibility = True
                locked = False
            case 'Poker Table':
                visibility = True
                locked = False
            case 'Croupier':
                visibility = True
                locked = True
            case 'Painting':
                visibility = True
                locked = True
            case 'Roulette Wheel':
                visibility = True
                locked = True
            case _:
                visibility = False
                locked = False

        objState = ObjectState(visibility, locked, "untouched")
        objState.object = obj
        roomState.object_states.append(objState)

    # generate a token to access this room during a server-game (should be stored in game server)
    token = roomState.generate_auth_token()

    db.session.add(roomState)
    db.session.commit()

    return {'token': token}


@bp.get('/examine')
@bp.auth_required(token_auth)
@bp.output(RoomStateOut, status_code=200)
def examine_room():
    # get room_state corresponding to token
    room_state = token_auth.current_user
   
    return room_state.assemble_json_room_state("Examining room")


@bp.get('/objects/<int:object_id>/examine')
@bp.auth_required(token_auth)
@bp.output(RoomStateOut, status_code=200)
def examine_object(object_id):
    # get room_state corresponding to token
    room_state = token_auth.current_user
    # get current state of given object
    current_object = db.get_or_404(Object, object_id)
    object_state = room_state.get_object_state_by_name(current_object.name)

    # GAME_LOGIC
    message = 'Nothing new'
    match current_object.name:
        case 'Slot Machine':
            message = 'The slot machine hints at a winning combination of symbols.'
        case 'Poker Table':
            if object_state.state == 'untouched':
                object_state.state = 'examined'
                message = 'You find a note with a clue about the slot machine.'
        case 'Croupier':
            message = 'The croupier is ready to play blackjack with you.'
        case 'Painting':
            message = 'Behind the painting, there is a hidden safe.'
        case _:
            message = 'You did not find anything new.'

    # update DB and return state to client
    db.session.commit()
    return room_state.assemble_json_room_state(message)


@bp.post('/objects/<int:object_id>/interact')
@bp.auth_required(token_auth)
@bp.input(InteractionIn, location='json')
@bp.output(RoomStateOut, status_code=200)
def interact_with_object(object_id, json_data):
    # get room_state corresponding to token
    room_state = token_auth.current_user
    # get current state of given object
    current_object = db.get_or_404(Object, object_id)
    object_state = room_state.get_object_state_by_name(current_object.name)
    
    # GAME_LOGIC
    message = ''
    match current_object.name:
        case 'Slot Machine':
            if json_data.get('parameter'):
                param_string = json_data.get('parameter')
                message = 'You pull the lever, but the symbols do not match.'
                if param_string == '777':
                    object_state.state = "unlocked"
                    message = 'The slot machine dings and reveals a hidden key!'
        case 'Croupier':
            if json_data.get('parameter') == 'win_blackjack':
                object_state.state = "friendly"
                message = 'You win the blackjack game. The croupier gives you a clue to a safe.'
        case 'Painting':
            if json_data.get('parameter'):
                param_string = json_data.get('parameter')
                message = 'You try to open the safe behind the painting.'
                if param_string == '1234':
                    object_state.state = "unlocked"
                    message = 'The safe clicks open, revealing another key!'
        case 'Roulette Wheel':
            if json_data.get('parameter'):
                param_string = json_data.get('parameter')
                message = 'You place your bet on the roulette wheel.'
                if param_string == '17':
                    room_state.exits_locked = False
                    object_state.state = "unlocked"
                    message = 'Congratulations! The roulette wheel lands on 17, unlocking the exit!'
        case _:
            message = 'Nothing happened.'

    # update DB and return state to client
    db.session.commit()
    return room_state.assemble_json_room_state(message)
